---
title: Home
sections:
  - section_id: hero
    type: section_hero
    subtitle: "IT Ops / Open Source Enthusiast / Always learning / Brazilian.\r\nKnowledge not shared is knowledge wasted!\r\n"
    image: >-
      https://i.pinimg.com/originals/89/74/34/897434458648858b690f487c3149ef6a.gif
  - title: Text
    section_id: intro
    type: section_content
  - title: Recent Posts
    section_id: posts
    type: section_posts
layout: advanced
---
